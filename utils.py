import logging
import pickle
from collections import defaultdict
from typing import List, Tuple

import numpy as np
import torch
from mmdet.core.bbox.geometry import bbox_overlaps
from mmdet.ops import nms

conf_type = Tuple[str, float]


def combine_boxes(preds: Tuple[np.ndarray, np.ndarray, List[str]], iou_thresh=0.5) -> Tuple[
    np.ndarray, List[List[conf_type]]]:
    boxes, probs, labels = preds
    if len(boxes) == 0:
        return np.empty((0, 4), dtype=np.float32), []
    labels = np.array(labels)

    if boxes.dtype != np.float32:
        logging.warning("AZAT, don't round the boxes!")
    boxes = boxes.astype(np.float32)
    boxes_and_probs = np.concatenate([boxes, np.expand_dims(probs, 1)], axis=1)
    post_nms, idxs = nms(boxes_and_probs, iou_thresh)
    overlaps = bbox_overlaps(torch.from_numpy(post_nms[:, :4]), torch.from_numpy(boxes))
    final_boxes = []
    final_classes = []
    for box, box_overlaps in zip(post_nms, overlaps):
        final_boxes.append(np.expand_dims(box[:4], 0))
        chosen_labels = labels[box_overlaps.numpy() > 0.5]
        chosen_probs = probs[box_overlaps.numpy() > 0.5]
        final_classes.append(list(zip(chosen_labels, chosen_probs)))
    final_boxes = np.concatenate(final_boxes)
    return final_boxes, final_classes


def combine_labels(nms_preds: List[List[conf_type]]) -> List[Tuple[conf_type, conf_type, conf_type]]:
    results = []
    for preds in nms_preds:
        size_labels = []
        for size in range(3, 0, -1):
            label_confs = defaultdict(int)
            for label, conf in preds:
                label = '.'.join(label.split('.')[:size])
                label_confs[label] = label_confs[label] + conf
            size_labels.append(max(label_confs.items(), key=lambda x: x[1]))
        results.append(size_labels)
    return results


if __name__ == '__main__':
    with open('frame_predictions.pkl', 'rb') as f:
        predictions = pickle.load(f)
    for img_path, img_boxes in predictions.items():
        img_boxes = combine_boxes(img_boxes)
    print(combine_preds(img_boxes[1]))
