import json
from pathlib import Path
import csv
import cv2
from tqdm import tqdm

import argparse

parser = argparse.ArgumentParser("Conver RTSD annotations to COCO format")
parser.add_argument("--keep-classes", action="store_true")
args = parser.parse_args()

data_root = Path('data')
data_path = data_root / 'rtsd'
annotations_path = data_path / ('annotations_all_classes.json' if args.keep_classes else 'annotations.json')
images_path = data_path / 'frames'

images = []
objects = []
added_images = {}

label_map = {}

with (data_path / 'full-gt.csv').open(newline='') as f:
    rows = list(csv.reader(f, delimiter=','))[1:]

for img_name, x_min, y_min, width, height, label, _ in tqdm(rows):
    for pat in ['_n', '_r']:
        label = label.split(pat)[0]
    x_min, y_min, width, height = map(int, [x_min, y_min, width, height])
    if img_name not in added_images:
        added_images[img_name] = len(added_images)
        img = cv2.imread(str(images_path / img_name))
        h, w, _ = img.shape
        images.append({
            "id":        added_images[img_name],
            "width":     w,
            "height":    h,
            "file_name": str((images_path / img_name).relative_to(data_root))
        })

    if args.keep_classes:
        if label not in label_map:
            label_map[label] = len(label_map) + 1
        category_id = label_map[label]
    else:
        category_id = 1

    objects.append({
        "id":          len(objects),
        "image_id":    added_images[img_name],
        "category_id": category_id,
        "area":        width * height,
        "bbox":        [x_min, y_min, width, height],
        "iscrowd":     0
    })

if len(label_map) == 0:
    label_map = {"sign": 1}
else:
    with (data_path / "label_map.json").open("w") as f:
        json.dump(label_map, f)

categories = [{"id":   idx,
               "name": label} for label, idx in label_map.items()]

with annotations_path.open('w') as f:
    json.dump({"images":      images,
               "annotations": objects,
               "categories":  categories}, f)
