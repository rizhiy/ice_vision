import os

import numpy as np
import cv2

import torch
from torch import nn
from torch.nn import functional as F

from albumentations import LongestMaxSize, PadIfNeeded, CenterCrop, Normalize, Compose
from albumentations.pytorch import ToTensor
from pretrainedmodels.models import resnet34


class Classifier(object):
    MEAN_VALUE = 127

    def __init__(self, weights_path, scale, n_labels):
        self.model = resnet34()
        n_features = self.model.last_linear.in_features
        self.model.last_linear = nn.Linear(n_features, n_labels)
        self.model = self.model.cuda().eval()
        self.n_labels = n_labels

        assert self.model.input_space == 'RGB'
        assert self.model.input_size == [3, 224, 224]
        assert self.model.input_range == [0, 1]

        normalize = Normalize(mean=self.model.mean, std=self.model.std)
        self.transform = Compose([LongestMaxSize(max_size=256),
                                  PadIfNeeded(256, 256),
                                  CenterCrop(224, 224),
                                  normalize,
                                  ToTensor()])

        state = torch.load(weights_path, map_location=lambda storage, loc: storage)
        self.model.load_state_dict(state['model'])
        self.scale = scale

    def _extract_patch(self, img, bbox):
        img_height, img_width, img_channels = img.shape
        xmin, ymin, xmax, ymax = bbox
        width = xmax - xmin
        height = ymax - ymin
        cx = xmin + width // 2
        cy = ymin + height // 2
        width = int(round(width * self.scale))
        height = int(round(height * self.scale))
        xmin = cx - width // 2
        ymin = cy - height // 2
        xmax = xmin + width
        ymax = ymin + height

        crop = img[max(ymin, 0):min(ymax, img_height), max(xmin, 0):min(xmax, img_width)]
        patch = np.full(shape=(height, width, img_channels), fill_value=self.MEAN_VALUE, dtype=img.dtype)
        pad_left = max(-xmin, 0)
        pad_top = max(-ymin, 0)
        patch[pad_top:pad_top + crop.shape[0], pad_left:pad_left + crop.shape[1]] = crop

        return patch

    def classify(self, img, bboxes):
        if len(bboxes) == 0:
            return np.zeros(shape=(0, self.n_labels), dtype=np.float32)

        input_tensor = []
        for bbox in bboxes:
            patch = self._extract_patch(img, bbox)
            patch = self.transform(image=patch)['image']
            input_tensor.append(patch)
        input_tensor = torch.stack(input_tensor)
        with torch.no_grad():
            input_tensor = input_tensor.cuda()
            class_probs = F.softmax(self.model(input_tensor), dim=1).cpu().numpy()

        return class_probs

index_map = {
        0: 5,
        1: 10,
        2: 20,
        3: 30,
        4: 40,
        5: 50,
        6: 60,
        7: 70,
        8: 80,
        9: 90,
        10: 100}

import os.path as osp
from tqdm import tqdm
import imageio

def add_numbers_to_tsv(tsv_path, root_folder, thresh):
    with open(tsv_path) as f:
        lines = f.readlines()
    
    header, *lines = lines
    
    model = Classifier('classifier/resnet34_3_24_n/snapshot_epoch_30.pth', 1.25, 11)
    for idx, line in enumerate(tqdm(lines, desc='calculating numbers')):
        img, x_min, y_min, x_max, y_max, cls, *_ = line.split('\t')
        if not cls.startswith('3.24'):
            lines[idx] = line.strip() + '\t' + '' + '\n'
            continue
        box = list(map(lambda x: int(float(x)), (x_min, y_min, x_max, y_max)))
        img_path = '/'.join(img.rsplit('_', 1)) + '.pnm'
        raw = imageio.imread(osp.join(root_folder, img_path))
        img = cv2.cvtColor(raw, cv2.COLOR_BAYER_BG2RGB)
        
        confs = model.classify(img, [box])[0]
        max_idx = np.argmax(confs)
        conf = confs[max_idx]
        if conf > thresh:
            number = index_map[max_idx]
            lines[idx] = line.strip() + '\t' + str(number) + '\n'
        else:
            lines[idx] = line.strip() + '\t' + '' + '\n'

    header = header.strip() + '\t' + 'data' + '\n'
    name, ext = tsv_path.rsplit('.', 1)
    with open(name + '_numbers'+ '.' + ext, 'w') as f:
        f.write(header)
        f.writelines(lines)

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('tsv_path', type=str)
    parser.add_argument('root_folder', type=str)
    parser.add_argument('--thresh', type=float, default=0.5)
    args = parser.parse_args()

    add_numbers_to_tsv(args.tsv_path, args.root_folder, args.thresh)
