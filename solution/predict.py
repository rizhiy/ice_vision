import os
import glob
import pickle
import json
import yaml

import cv2
from tqdm import tqdm

from torch.utils.data import DataLoader, Sampler

from dataset import IceVisionDataset
from predictor import Predictor

from mmdet.datasets import to_tensor

from mmdet.datasets.transforms import ImageTransform

ROOT = os.path.dirname(os.path.abspath(__file__))
CONFIG_PATH = os.path.join(ROOT, 'config.yaml')
PREDICTIONS_PATH_TEMPLATE = os.path.join(ROOT, 'predictions', 'frame_predictions_{}.pkl')

DBG_SHOW = False


class StepSampler(Sampler):

    def __init__(self, data_source, step, start=0):
        super(StepSampler, self).__init__(data_source)
        self.data_source = data_source
        assert step > 0
        self.step = step
        self.start = start

    def __iter__(self):
        return iter(range(self.start, len(self.data_source), self.step))

    def __len__(self):
        return len(range(self.start, len(self.data_source), self.step))


def main():
    assert os.path.abspath(os.getcwd()) == ROOT, 'Please run this script from the {} folder'.format(ROOT)

    predictions_root = os.path.dirname(PREDICTIONS_PATH_TEMPLATE)
    os.makedirs(predictions_root, exist_ok=True)
    print('Destination path template: {}'.format(PREDICTIONS_PATH_TEMPLATE))

    with open(CONFIG_PATH, 'r') as f:
        config = yaml.load(f)

    predictor = Predictor(config)
    cfg = predictor._detector.model.cfg
    device = next(predictor._detector.model.parameters()).device

    img_transform = ImageTransform(size_divisor=cfg.data.test.size_divisor, **cfg.img_norm_cfg)

    test_sequences = []
    for seq_name in os.listdir(config['IMAGES_ROOT']):
        for suffix in ['left', 'right']:
            seq_name_with_suffix = os.path.join(seq_name, suffix)
            if os.path.exists(os.path.join(config['IMAGES_ROOT'], seq_name_with_suffix)):
                test_sequences.append(seq_name_with_suffix)

    test_sequences = sorted(test_sequences)
    print('Testing sequences: {}'.format(test_sequences))

    dataset = IceVisionDataset(config['IMAGES_ROOT'], test_sequences, img_transform, cfg)
    print('Total number of files: {}.'.format(len(dataset)))

    snapshots = [(int(path.split('frame_predictions_')[-1].split('.')[0]), path) for path in
                 os.listdir(predictions_root)
                 if path.startswith('frame_predictions_') and path.endswith('.pkl')]
    if len(snapshots) > 0:
        snapshots.sort(key=lambda t: t[0])
        print('Continue from {}'.format(snapshots[-1][1]))

        with open(os.path.join(predictions_root, snapshots[-1][1]), 'rb') as f:
            img_name_to_predictions = pickle.load(f)

        start = snapshots[-1][0] + 1
    else:
        img_name_to_predictions = {}
        start = 0

    sampler = StepSampler(dataset, config['STEP'], start)
    loader = DataLoader(dataset, batch_size=1, sampler=sampler, num_workers=config['NUM_WORKERS'],
                        collate_fn=lambda X: X)

    if DBG_SHOW:
        cv2.namedWindow('frame', cv2.WINDOW_NORMAL)

    for i, samples in zip(tqdm(range(start, len(dataset), config['STEP'])), loader):
        img_name = samples[0]['name']
        img = samples[0]['image']
        img['img'][0] = to_tensor(img['img'][0]).to(device).unsqueeze(0)

        bboxes, scores, categories = predictor.predict(img)

        img_name_to_predictions[img_name] = (bboxes, scores, categories)
        if i % config['SAVE_FREQUENCY'] == 0 and i > 0:
            predictions_path = PREDICTIONS_PATH_TEMPLATE.format(i)
            assert not os.path.exists(predictions_path)
            print('Dumping predictions to {}'.format(predictions_path))
            with open(predictions_path, 'wb') as f:
                pickle.dump(img_name_to_predictions, f)

        if DBG_SHOW:
            for bbox, score, category in zip(bboxes, scores, categories):
                if score > 0.65:
                    xmin, ymin, xmax, ymax = bbox
                    img = cv2.rectangle(img, (xmin, ymin), (xmax, ymax), (0, 255, 0), 10)
                    cv2.putText(img, '{} ({:.3f}'.format(category, score), (xmin, ymin), cv2.FONT_HERSHEY_SIMPLEX, 1.2,
                                (255, 255, 255), 3)
            cv2.imshow('frame', img)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    with open(PREDICTIONS_PATH_TEMPLATE.format('all'), 'wb') as f:
        pickle.dump(img_name_to_predictions, f)


if __name__ == '__main__':
    main()
