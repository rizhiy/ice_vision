import os
import imageio
import cv2
cv2.setNumThreads(0)

from torch.utils.data import Dataset


def _prepare_data(img, img_transform, cfg):
    ori_shape = img.shape
    img, img_shape, pad_shape, scale_factor = img_transform(
        img,
        scale=cfg.data.test.img_scale,
        keep_ratio=cfg.data.test.get('resize_keep_ratio', True))
    img_meta = [
        dict(
            ori_shape=ori_shape,
            img_shape=img_shape,
            pad_shape=pad_shape,
            scale_factor=scale_factor,
            flip=False)
    ]
    return dict(img=[img], img_meta=[img_meta])


class IceVisionDataset(Dataset):
    def __init__(self, images_root, sequences, transform, cfg):
        self.content = []
        for sequence in sequences:
            for img_name in os.listdir(os.path.join(images_root, sequence)):
                if img_name.endswith('.pnm'):
                    self.content.append(os.path.join(sequence, img_name))

        def sort_key(img_path):
            parts = img_path.split('/')
            idx = int(parts[-1].split('.')[0])
            return parts[0], idx

        self.content = sorted(self.content, key=sort_key)
        self.images_root = images_root
        self.transform = transform

        self.cfg = cfg

    def __len__(self):
        return len(self.content)

    def __getitem__(self, idx):
        rel_path = self.content[idx]
        path = os.path.join(self.images_root, rel_path)
        assert path.endswith('.pnm'), path
        raw = imageio.imread(path)
        img = cv2.cvtColor(raw, cv2.COLOR_BAYER_BG2RGB)

        img = _prepare_data(img, self.transform, self.cfg)

        return {'image': img, 'name': rel_path}
