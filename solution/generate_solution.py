import os
import pickle
import yaml
from collections import defaultdict

import numpy as np

from utils import combine_boxes, combine_labels, track_iou

ROOT = os.path.dirname(os.path.abspath(__file__))
CONFIG_PATH = os.path.join(ROOT, 'config.yaml')
PREDICTIONS_PATH = os.path.join(ROOT, 'predictions', 'frame_predictions_all.pkl')
SOLUTION_PATH = os.path.join(ROOT, 'solution.tsv')

N_LEVELS = 3


def inter_track(boxes, frame_idxs):
    first_frame = frame_idxs[0]
    last_frame = frame_idxs[-1]
    frames_to_interpolate = list(range(first_frame, last_frame + 1))
    interp_track = np.zeros((len(frames_to_interpolate), 4))

    for i in range(4):
        interp_track[:, i] = np.interp(frames_to_interpolate, frame_idxs, boxes[:, i])

    return interp_track, frames_to_interpolate


def main():
    assert os.path.abspath(os.getcwd()) == ROOT, 'Please run this script from the {} folder'.format(ROOT)
    assert os.path.exists(PREDICTIONS_PATH), PREDICTIONS_PATH

    print('Destination path: {}'.format(SOLUTION_PATH))

    with open(CONFIG_PATH, 'r') as f:
        config = yaml.load(f)


    with open(PREDICTIONS_PATH, 'rb') as f:
        img_name_to_predictions = pickle.load(f)

    img_name_to_predictions = {img_name.replace('/left', '_left').replace('/right', '_right').replace('.pnm', ''): predictions for
                               img_name, predictions in img_name_to_predictions.items()}

    content = sorted(img_name_to_predictions)

    img_name_to_bboxes_with_predictions_list = {}
    for img_name in content:
        predictions = img_name_to_predictions[img_name]
        bboxes, predictions_list = combine_boxes(predictions)
        predictions_list = combine_labels(predictions_list)
        img_name_to_bboxes_with_predictions_list[img_name] = (bboxes, predictions_list)

    seq_name_to_bboxes_with_predictions_list_seq = defaultdict(list)
    for img_name, bboxes_with_predictions_list in img_name_to_bboxes_with_predictions_list.items():
        seq_name, idx = img_name.split('/')
        idx = int(idx)
        seq_name_to_bboxes_with_predictions_list_seq[seq_name].append((idx, bboxes_with_predictions_list))

    print('Total number of sequences: {}'.format(len(seq_name_to_bboxes_with_predictions_list_seq)))

    img_name_to_tracked_predictions = {}
    for seq_name in seq_name_to_bboxes_with_predictions_list_seq:
        bboxes_with_predictions_list_seq = seq_name_to_bboxes_with_predictions_list_seq[seq_name]
        bboxes_with_predictions_list_seq = sorted(bboxes_with_predictions_list_seq)
        print('Sequence {} length: {}'.format(seq_name, len(bboxes_with_predictions_list_seq)))

        frame_to_predictions = defaultdict(list)

        frame_to_dets = []
        frame_to_bbox_to_predictions = []
        frame_indices = []
        for idx, bboxes_with_predictions_list in bboxes_with_predictions_list_seq:
            frame_indices.append(idx)
            bboxes, predictions_list = bboxes_with_predictions_list

            dets = []
            bbox_to_predictions = {}
            for bbox, predictions in zip(bboxes, predictions_list):
                score = predictions[0][1]
                if score > config['TRACKER']['DET_LOW_THRESH']:
                    bbox = tuple(bbox.tolist())
                    assert bbox not in bbox_to_predictions
                    bbox_to_predictions[bbox] = predictions
                    dets.append({'bbox': bbox, 'score': score})
            frame_to_dets.append(dets)
            frame_to_bbox_to_predictions.append(bbox_to_predictions)

        tracks = track_iou(frame_to_dets,
                           config['TRACKER']['DET_LOW_THRESH'],
                           config['TRACKER']['DET_HIGH_THRESH'],
                           config['TRACKER']['IOU_THRESH'],
                           config['TRACKER']['MIN_LENGTH'])

        for track in tracks:
            start_frame = track['start_frame'] - 1
            bboxes = track['bboxes']

            predictions = []
            for level in range(N_LEVELS):
                category_to_score = defaultdict(float)
                for i, bbox in enumerate(bboxes):
                    frame = start_frame + i
                    category, score = frame_to_bbox_to_predictions[frame][bbox][level]
                    category_to_score[category] += score

                categories = sorted(category_to_score)
                scores = np.array([category_to_score[category] for category in categories], dtype=np.float64)
                best_idx = scores.argmax()
                best_score = scores[best_idx]
                best_category = categories[best_idx]
                predictions.append((best_category, best_score / len(bboxes)))

            track_frame_indices = np.array([frame_indices[start_frame + i] for i in range(len(bboxes))])
            bboxes = np.array(bboxes)
            interp_bboxes, inter_frames = inter_track(bboxes, track_frame_indices)

            for i, bbox in enumerate(interp_bboxes):
                frame = inter_frames[i]
                frame_to_predictions[frame].append((bbox, predictions))

        frames = sorted(frame_to_predictions)
        for frame in frames:
            img_name = seq_name + '/' + str(frame).zfill(6)
            img_name_to_tracked_predictions[img_name] = frame_to_predictions[frame]

    content = sorted(img_name_to_tracked_predictions)

    with open(SOLUTION_PATH, 'w') as f:
        f.write('frame\txtl\tytl\txbr\tybr\tclass\n')
        for img_name in content:
            tracked_predictions = img_name_to_tracked_predictions[img_name]

            for bbox, predictions in tracked_predictions:
                xmin, ymin, xmax, ymax = bbox
                if predictions[0][1] > config['SOLUTION']['THRESH_3']:
                    category = predictions[0][0]
                elif predictions[1][1] > config['SOLUTION']['THRESH_2']:
                    category = predictions[1][0]
                elif predictions[2][1] > config['SOLUTION']['THRESH_1']:
                    category = predictions[2][0]
                else:
                    continue

                f.write('\t'.join([img_name, str(xmin), str(ymin), str(xmax), str(ymax), category]) + '\n')

    print('Done!')


if __name__ == '__main__':
    main()

