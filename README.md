This repository contains code that was used to achieve first place in [IceVision Challenge](http://icevision.upgreat.one/).

The team was Azat Davletshin and Artem Vasenin.

# Installation

Run `bash make.sh`

# Final solution

Final solution is located in `solution` folder, to run it use `run.sh`