from mmdet.apis import init_detector, inference_detector

import argparse

from pathlib import Path
import pickle
from tqdm import tqdm

parser = argparse.ArgumentParser("Run detector on images in folder")
parser.add_argument("cfg", type=Path)
parser.add_argument("folder", type=Path)
args = parser.parse_args()

ckpt_path = Path("output") / args.cfg.stem / "latest.pth"

model = init_detector(str(args.cfg), str(ckpt_path), device='cuda:0')

results = {}

for img_path in tqdm(list(args.folder.rglob('*.jpg'))):
    results[str(img_path)] = inference_detector(model, str(img_path))

with (args.folder / "results.pkl").open('wb') as f:
    pickle.dump(results, f)

