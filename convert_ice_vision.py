import argparse
import json
import shutil
from pathlib import Path

import cv2
from tqdm import tqdm

orig_clss = ['2.1', '2.4', '3.1', '3.24', '3.27', '4.1', '4.2', '5.19', '5.20', '8.22']

parser = argparse.ArgumentParser("Convert ice_vision annotations to COCO format")
parser.add_argument("--class-modify", type=str, default='', choices=['orig_only', 'orig_unfold', 'super'])
parser.add_argument("--size", type=int, nargs=2)
parser.add_argument("--pnm", action="store_true")
args = parser.parse_args()

ignore_classes = 'N/A'

data_root = Path('data')
dataset_path = data_root / 'ice_vision'
annotations_path = dataset_path / 'annotations'
orig_frames_path = dataset_path / 'orig_frames'
jpeg_frames_path = dataset_path / 'frames'
pnm_frames_path = dataset_path / 'pnm_frames'

splits_map = {}
with open('orig_splits.json') as f:
    splits = json.load(f)
for split_name, values in splits.items():
    for name in values:
        splits_map[name] = split_name

anns = {}
for split_name in splits.keys():
    anns[split_name] = {"images": [], "annotations": []}

label_map = {}

sizes = set()

for ann_path in tqdm(list(annotations_path.rglob('*.tsv'))):
    split = splits_map.get(ann_path.parent.stem, 'new')
    img_path = orig_frames_path / ann_path.relative_to(annotations_path)
    img_path = Path('/'.join(str(img_path).rsplit('_', 1))).with_suffix('.pnm')
    if args.pnm:
        new_img_path = pnm_frames_path / img_path.relative_to(orig_frames_path)
    else:
        new_img_path = pnm_frames_path / img_path.relative_to(orig_frames_path).with_suffix('.jpg')
    new_img_path.parent.mkdir(exist_ok=True, parents=True)
    if args.size is not None:
        height, width = args.size
    else:
        img = cv2.imread(str(img_path), -1)
        if img is None:
            continue
        height, width, *_ = img.shape
        sizes.add((height, width))

        if args.pnm:
            shutil.copyfile(img_path, new_img_path)
        else:
            img = cv2.cvtColor(img, cv2.COLOR_BAYER_BG2RGB)
            cv2.imwrite(str(new_img_path), img)

    img_id = len(anns[split]['images'])
    anns[split]['images'].append({
        "id": img_id,
        "width": width,
        "height": height,
        "file_name": str(new_img_path.relative_to(data_root))
    })

    with ann_path.open() as f:
        lines = [x.strip() for x in f.readlines()]
    for line in lines[1:]:
        label, *rest = line.split('\t')
        if label in ['N/A', 'NA']:
            continue
        if args.class_modify == 'super':
            label = label.split('.')[0]
        if args.class_modify == 'orig_only':
            for cls in orig_clss:
                if label.startswith(cls):
                    label = cls
                    break
            else:
                continue
        if args.class_modify == 'orig_unfold':
            for cls in orig_clss:
                if label.startswith(cls):
                    break
            else:
                continue

        if label not in label_map:
            label_map[label] = len(label_map) + 1

        x_min, y_min, x_max, y_max = map(int, rest[:4])
        anns[split]['annotations'].append({
            "id": len(anns[split]['annotations']),
            "image_id": img_id,
            "category_id": label_map[label],
            "area": (x_max - x_min) * (y_max - y_min),
            "bbox": [x_min, y_min, x_max - x_min, y_max - y_min],
            "iscrowd": 0
        })

categories = [{"id": idx, "name": cls_name} for cls_name, idx in label_map.items()]

for split_name, split_anns in anns.items():
    save_name = split_name
    if args.class_modify != '':
        save_name += '_' + args.class_modify
    if args.pnm:
        save_name += '_pnm'
    split_anns['categories'] = categories
    with open(annotations_path / f"{save_name}.json", 'w') as f:
        json.dump(split_anns, f)

label_map_name = 'label_map'
if args.class_modify:
    label_map_name += '_' + args.class_modify
if args.pnm:
    label_map_name += '_pnm'

with open(annotations_path / f'{label_map_name}.json', 'w') as f:
    json.dump(label_map, f)

if len(sizes) > 0:
    print(f"image sizes: {sizes}")
